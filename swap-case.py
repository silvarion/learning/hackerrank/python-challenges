def swap_case(s):
    result = ""
    for i in range(len(s)):
        if s[i].lower() == s[i]:
            result += s[i].upper()
        elif s[i].upper() == s[i]:
            result += s[i].lower()
    return result

