if __name__ == '__main__':
    n = int(input())
    arr = map(int, input().split())

lst=list(arr)
for i in range(len(lst)):
    # First iteration >> Initialize
    if i == 0:
        max = lst[i]
        rup = lst[i]
    # After initialization
    else:
        # If just initialized
        if max == rup:
            # If new max found
            if lst[i] > max:
                max = lst[i]
            # No new max, new runner-up?
            else:
                rup = lst[i]
        else:
            if lst[i] > max:
                rup = max
                max = lst[i]
            if lst[i] > rup and lst[i] < max:
                rup = lst[i]
print(rup)
