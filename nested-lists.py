if __name__ == '__main__':
    grades=[]
    for _ in range(int(input())):
        name = input()
        score = float(input())
        grades.append([name,score])
    min1 = 100.0
    min2 = 100.0
    for item in grades:
        if item[1] < min1:
            if min1 < min2:
                min2 = min1
            min1 = item[1]
        else:
            if item[1] < min2 and item[1] != min1:
                min2 = item[1]
    # Build result set
    to_print = []
    for item in grades:
        if item[1] == min2:
            to_print.append(item[0])
    # Print results
    to_print.sort()
    for name in to_print:
        print(name)
