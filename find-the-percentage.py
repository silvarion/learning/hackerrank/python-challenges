if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()
    grade_count = 0
    total = 0.0
    for grade in student_marks[query_name]:
        grade_count += 1
        total += grade
    grade_avg = total/grade_count
    print(f"{grade_avg:.2f}")
