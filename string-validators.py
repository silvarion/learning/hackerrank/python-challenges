from string import ascii_lowercase, ascii_uppercase, digits

def check_contents(word):
    has_uppers = False
    has_lowers = False
    has_digits = False
    for character in word:
        if not has_lowers and character not in ascii_lowercase:
            has_lowers = False
        else:
            has_lowers = True
        if not has_uppers and character not in ascii_uppercase:
            has_uppers = False
        else:
            has_uppers = True
        if not has_digits and character not in digits:
            has_digits = False
        else:
            has_digits = True
    return has_uppers, has_lowers, has_digits        


if __name__ == '__main__':
    s = input()
    has_uppers, has_lowers, has_digits = check_contents(s)
    
    if has_uppers or has_lowers or has_digits:
        print(True)
    else:
        print(False)
    if has_uppers or has_lowers:
        print(True)
    else:
        print(False)
    if has_digits:
        print(True)
    else:
        print(False)
    if has_lowers:
        print(True)
    else:
        print(False)
    if has_uppers:
        print(True)
    else:
        print(False)
