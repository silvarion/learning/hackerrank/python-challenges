# Python Challenges

This repository is meant to hold my solutions to HackerRank's Python Challenges

Challenge                       | Level     | Solution
------------------------------- |-----------|--------------------------------------------
Arithmetic Operators            | Easy      | [Code](arithmetic-operators.py)
Find a string                   | Easy      | [Code](find-a-string.py)
Find the percentage             | Easy      | [Code](find-the-percentage.py)
Find the Runner-Up Score!       | Easy      | [Code](find-the-runner-upp-score.py)
Lists                           | Easy      | [Code](lists.py)
List Comprehensions             | Easy      | [Code](list-comprehensions.py)
Loops                           | Easy      | [Code](loops.py)
Mutations                       | Easy      | [Code](mutations.py)
Nested Lists                    | Easy      | [Code](nested-lists.py)
Print Function                  | Easy      | [Code](print-function.py)
Python If-Else                  | Easy      | [Code](python-if-else.py)
Python Division                 | Easy      | [Code](python-division.py)
Tuples                          | Easy      | [Code](tuples.py)
Say "Hello World!" with Python  | Easy      | [Code](say-hello-world-with-python.py)
String Split and Join           | Easy      | [Code](string-split-and-join.py)
String Validators               | Easy      | [Code](string-validators.py)
sWAP cASE                       | Easy      | [Code](swap-case.py)
Text Alignment                  | Easy      | [Code](text-alignment.py)
What's Your Name?               | Easy      | [Code](whats-your-name.py)
Write a function                | Medium    | [Code](write-a-function.py)
